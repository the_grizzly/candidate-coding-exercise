# Candidate Live Coding Exercise #
Hello and Welcome! 

Firstly, we'd like to thank you for taking time to take this 
exercise and don't worry, it's not a huge task as we only would like to find out 
your approach to coding, problem solving and use of tools. There are no tricks,
but little hints for you if you can find them!

In this exercise we have a Spring Boot App that has a Jersey resource and a H2 Database
with Spring Transactions. It adds and gets jobs, and does to same for job actions. A job can have multiple actions. In database terms, Job 1--* Action

You can take 5 minutes or so to familirise yourself with the code. And once you
have done that, let us know and you can start with the tasks below. Please do pay attention to the task description as it has everything you need to accomplish it. As always, if you have any questions feel free to ask.

1. Create a new branch. You can use your name as the new branch name 
1. See if you can spot anything wrong with the code
1. Create a JAR file using Maven from the *terminal*
1. Run the jar file and direct it's logs to a log file using in the terminal
1. Open a second terminal window and tail the logs
1. Get all the jobs by hitting the _/api/v1/jobs_ endpoint
1. Add a new job by hitting the same endpoint
1. Try getting all the jobs to make sure your job has been saved
1. If all goes well, go back to the code and implement an endpoint that lists all
actions for a given job ID
1. Now you need to test your new API but before
    1. Create a new job and action by using the _/api/v1/jobsActions_ endpoint
    1. Add another action for the new job
    1. List all the actions for the new job using the newly implemented API
