DROP TABLE IF EXISTS `job`;
CREATE TABLE `job`
(
    job_id         INT AUTO_INCREMENT PRIMARY KEY,
    description       VARCHAR(250),
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS `action`;
CREATE TABLE `action`
(
    action_id         INT AUTO_INCREMENT PRIMARY KEY,
    job_id            INT NOT NULL,
    action_name       VARCHAR(250),
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)
