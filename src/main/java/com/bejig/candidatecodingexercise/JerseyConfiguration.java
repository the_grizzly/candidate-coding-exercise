package com.bejig.candidatecodingexercise;

import com.bejig.candidatecodingexercise.api.JobActionResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

/**
 * @author vedat
 */
@Component
@ApplicationPath("/api")
public class JerseyConfiguration extends ResourceConfig {
    public JerseyConfiguration() {
        register(JobActionResource.class);
    }
}
