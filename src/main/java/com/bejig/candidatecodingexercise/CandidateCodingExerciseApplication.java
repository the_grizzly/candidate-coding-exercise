package com.bejig.candidatecodingexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EntityScan("com.bejig.candidatecodingexercise.model")
public class CandidateCodingExerciseApplication {

    public static void main(String[] args) {
        SpringApplication.run(CandidateCodingExerciseApplication.class, args);
    }
}
