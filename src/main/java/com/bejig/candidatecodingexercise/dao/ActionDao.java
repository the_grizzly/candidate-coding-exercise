package com.bejig.candidatecodingexercise.dao;

import com.bejig.candidatecodingexercise.model.Action;

import java.util.List;

/**
 * @author vedat
 */
public interface ActionDao {
    Action save(Action action);

    List<Action> getByJobId(Long jobId);
}
