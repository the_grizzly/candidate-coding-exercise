package com.bejig.candidatecodingexercise.dao;

import com.bejig.candidatecodingexercise.model.Job;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author vedat
 */
@Repository
public class JobDaoImpl implements JobDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Job> findAll() {
        return entityManager.createQuery("from Job").getResultList();
    }

    @Override
    public Job save(Job job) {
        entityManager.persist(job);
        return job;
    }

    public Job findById(Long jobId) {
        return entityManager.find(Job.class, jobId);
    }
}
