package com.bejig.candidatecodingexercise.dao;

import com.bejig.candidatecodingexercise.model.Job;

import java.util.List;

/**
 * @author vedat
 */
public interface JobDao {

    List<Job> findAll();

    Job save(Job job);

    Job findById(Long jobId);
}
