package com.bejig.candidatecodingexercise.dao;

import com.bejig.candidatecodingexercise.model.Action;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author vedat
 */
@Repository
public class ActionDaoImpl implements ActionDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Action save(Action action) {
        entityManager.persist(action);
        return action;
    }

    @Override
    public List<Action> getByJobId(Long jobId) {
        return entityManager.createQuery("FROM Action WHERE jobId = :jobId").setParameter("jobId", jobId).getResultList();
    }
}
