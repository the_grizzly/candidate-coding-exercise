package com.bejig.candidatecodingexercise.api;

import java.io.Serializable;

/**
 * @author vedat
 */
public class JobActionRequest implements Serializable {
    private String jobDescription;
    private String actionName;

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }
}
