package com.bejig.candidatecodingexercise.api;

import com.bejig.candidatecodingexercise.model.Action;
import com.bejig.candidatecodingexercise.model.Job;

/**
 * @author vedat
 */
public class JobActionResponse {
    private Job job;
    private Action action;

    public JobActionResponse() {}

    public JobActionResponse(Job job, Action action) {
        this.job = job;
        this.action = action;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
