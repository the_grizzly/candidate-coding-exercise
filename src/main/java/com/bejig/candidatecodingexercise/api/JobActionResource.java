package com.bejig.candidatecodingexercise.api;

import com.bejig.candidatecodingexercise.model.Action;
import com.bejig.candidatecodingexercise.model.Job;
import com.bejig.candidatecodingexercise.service.JobActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.List;

/**
 * @author vedat
 */
@Component
@Path("/v1")
public class JobActionResource {

    @Autowired
    private JobActionService jobActionService;

    @GET
    @Produces("application/json")
    @Path("/jobs")
    public List<Job> getAllJobs() {
        return jobActionService.getAllJobs();
    }

    @GET
    @Produces("application/json")
    @Path("/jobs/{jobId}")
    public Job getJob(@PathParam(value = "jobId") Long jobId) {
        return jobActionService.getJobById(jobId);
    }

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/jobs")
    @PostMapping("/jobs")
    public Job createJob(Job job) {
        return jobActionService.addJob(job.getDescription());
    }

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/jobs/{jobId}/actions/")
    @PostMapping("/jobs/{jobId}/actions/")
    public Action createAction(@PathParam("jobId") Long jobId, Action action) {
        return jobActionService.addAction(jobId, action.getActionName());
    }

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/jobsActions")
    @PostMapping("/jobsActions")
    public JobActionResponse createJobAndAction(JobActionRequest jobActionRequest) {
        Action action = jobActionService.addJobAndAction(jobActionRequest.getJobDescription(), jobActionRequest.getActionName());
        Job job = jobActionService.getJobById(action.getJobId());

        return new JobActionResponse(job, action);
    }
}
