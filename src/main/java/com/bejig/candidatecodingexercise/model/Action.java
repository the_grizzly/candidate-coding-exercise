package com.bejig.candidatecodingexercise.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author vedat
 */
@Entity
public class Action {
    @Id
    @GeneratedValue
    @Column(nullable = false)
    private Long actionId;
    @Column(nullable = false)
    private Long jobId;
    @Column(nullable = false)
    private String actionName;
    @Column
    private Date createdTimestamp;

    public Action() {}

    public Action(Long actionId, Long jobId, String actionName) {
        this.actionId = actionId;
        this.jobId = jobId;
        this.actionName = actionName;
    }

    public Long getActionId() {
        return actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public Date getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(Date createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }


}
