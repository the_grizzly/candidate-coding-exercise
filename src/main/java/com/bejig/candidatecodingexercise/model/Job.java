package com.bejig.candidatecodingexercise.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author vedat
 */
@Entity
public class Job {
    @Id
    @GeneratedValue
    @Column(nullable = false)
    private Long jobId;
    @Column(nullable = false)
    private String description;
    @Column
    private Date createdTimestamp;

    public Job() {}

    public Job(String description) {
        this.description = description;
        this.createdTimestamp = new Date();
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(Date createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    @Override
    public String toString() {
        return "Job{" +
                "jobId=" + jobId +
                ", description='" + description + '\'' +
                ", createdTimestamp=" + createdTimestamp +
                '}';
    }
}
