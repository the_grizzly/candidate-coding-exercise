package com.bejig.candidatecodingexercise.service;

import com.bejig.candidatecodingexercise.model.Action;
import com.bejig.candidatecodingexercise.model.Job;

import java.util.List;

/**
 * @author vedat
 */
public interface JobActionService {
    List<Job> getAllJobs();

    Job addJob(String description);

    Job getJobById(Long jobId);

    Action addAction(Long jobId, String actionName);

    Action addJobAndAction(String jobDescription, String actionName);
}
