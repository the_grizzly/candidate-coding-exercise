package com.bejig.candidatecodingexercise.service;

import com.bejig.candidatecodingexercise.dao.ActionDao;
import com.bejig.candidatecodingexercise.dao.JobDao;
import com.bejig.candidatecodingexercise.model.Action;
import com.bejig.candidatecodingexercise.model.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author vedat
 */
@Service
public class JobActionServiceImpl implements JobActionService {
    private static final Logger logger = LoggerFactory.getLogger(JobActionServiceImpl.class);

    private final JobDao jobDao;
    private final ActionDao actionDao;

    public JobActionServiceImpl(@Autowired  JobDao jobDao, @Autowired ActionDao actionDao) {
        this.jobDao = jobDao;
        this.actionDao = actionDao;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<Job> getAllJobs() {
        logger.info("Get all jobs");
        return jobDao.findAll();
    }

    @Override
    @Transactional
    public Job addJob(String description) {
        logger.info("add job for " + description);
        Job job = new Job(description);
        return jobDao.save(job);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Job getJobById(Long jobId) {
        logger.info("get job by id " + jobId);
        return jobDao.findById(jobId);
    }

    @Override
    @Transactional
    public Action addAction(Long jobId, String actionName) {
        logger.info("add action " + actionName + " for jobId " + jobId);
        Action action = new Action(null, jobId, actionName);
        return actionDao.save(action);
    }

    @Override
    public Action addJobAndAction(String jobDescription, String actionName) {
        logger.info("add action " + actionName + " for new job " + jobDescription);
        Job job = addJob(jobDescription);
        return addAction(job.getJobId(), actionName);
    }
}
