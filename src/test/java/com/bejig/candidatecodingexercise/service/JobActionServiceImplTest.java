package com.bejig.candidatecodingexercise.service;

import com.bejig.candidatecodingexercise.CandidateCodingExerciseApplication;
import com.bejig.candidatecodingexercise.model.Action;
import com.bejig.candidatecodingexercise.model.Job;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = CandidateCodingExerciseApplication.class)
public class JobActionServiceImplTest {

    @Autowired
    private JobActionService jobActionService;

    @Test
    public void testJobs() {
        String jobName = "First Job";
        Job job = jobActionService.addJob(jobName);
        assertNotNull(job);
        assertNotNull(job.getJobId());

        Job jobById = jobActionService.getJobById(job.getJobId());
        assertNotNull(jobById);
        assertEquals(jobName, jobById.getDescription());

        List<Job> allJobs = jobActionService.getAllJobs();
        assertNotNull(allJobs);
        assertFalse(allJobs.isEmpty());
    }

    @Test
    public void testActions() {
        Job job = jobActionService.addJob("Action Job");
        assertNotNull(job);
        assertNotNull(job.getJobId());

        Action action = jobActionService.addAction(job.getJobId(), "First Action");
        assertNotNull(action);
        assertNotNull(action.getActionId());
        assertEquals(job.getJobId(), action.getJobId());
    }

    @Test
    public void testAddingJobAndActionTogether() {
        String actionName = "First Together Action";
        String jobDescription = "Together Job";
        Action action = jobActionService.addJobAndAction(jobDescription, actionName);

        assertNotNull(action);
        assertNotNull(action.getJobId());
        assertNotNull(action.getActionId());
        assertEquals(actionName, action.getActionName());

        Job jobById = jobActionService.getJobById(action.getJobId());
        assertNotNull(jobById);
        assertEquals(jobDescription, jobById.getDescription());
    }

}
